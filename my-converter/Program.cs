﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace my_converter
{
    // derived from https://superdry.apphb.com/tools/online-rsa-key-converter

    class Program
    {
        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText(args[0]);
            Console.WriteLine(RsaKeyConverter.PemToXml(text));
            Console.ReadKey();
        }
    }
}
