﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace my_converter
{
	public static class RsaExtensions
	{
		public static AsymmetricCipherKeyPair GetKeyPair(this RSA rsa)
		{
			try
			{
				return DotNetUtilities.GetRsaKeyPair(rsa);
			}
			catch
			{
				return null;
			}
		}

		public static RsaKeyParameters GetPublicKey(this RSA rsa)
		{
			try
			{
				return DotNetUtilities.GetRsaPublicKey(rsa);
			}
			catch
			{
				return null;
			}
		}
	}
}
