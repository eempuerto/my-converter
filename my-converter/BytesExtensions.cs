﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace my_converter
{
	public static class BytesExtensions
	{
		public static string ToBase64(this byte[] bytes)
		{
			return Convert.ToBase64String(bytes);
		}
	}
}
